#include <stdio.h>
#include <stdlib.h>

void tablero(int altura,int anchura);

int main(int argc, char *argv[]){
    if(argc<=1){
        printf("\n\t****************\n\t* Matrix Frame *\n\t****************\n");
        printf("\n\tUsage: af <height> <widht>");
        printf("\n\tExample: af 6 5\n\n");
        return 0;
    }
    int altura=(int)strtol(argv[1],NULL,10);
    int anchura=(int)strtol(argv[2],NULL,10);
    //Call to function tablero
    tablero(altura, anchura);
    return 0;
}

// Fill in and show the board
void tablero(int altura, int anchura){
    int i,j;
    char tabla[altura][anchura];
    // Check if the array is already filled
    if(tabla[0][0]!='#'){
        // Create the frame with '#' and fill it with spaces
        for(i=0;i<altura;i++){
            for(j=0;j<anchura;j++){
                // Check if it is in row 0 to fill all complete
                if(i<1 || i==altura-1){
                    tabla[i][j]='#';
                }else{
                    /* Check if the row is different from the first and also,
                    it also verifies if it is the column 0 to only fill the sides */
                    if(i>0 && j<1){
                        tabla[i][0]='#';
                        tabla[i][anchura-1]='#';
                    }else{
                        // Fill in all non-borders with spaces
                        tabla[i][j]=' ';
                        /* Ends the cycle when the column reaches an anterior position of the right edge */
                        if(j==anchura-2){
                            break;
                        }
                    }
                }
            }
        }
    }
    //Show the Matrix
    for(i=0;i<altura;i++){
        for(j=0;j<anchura;j++){
            printf("%c",tabla[i][j]);
        }
        printf("\n");
    }
}
