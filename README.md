
# Matrix-Frame
## Menu
- [English](https://gitlab.com/jorgesanux/Matrix-Frame#english)
	- Usage
		- af
		- af \<height> \<width>
- [Español](https://gitlab.com/jorgesanux/Matrix-Frame#espa%C3%B1ol)
	- Uso
		- af
		- af \<height> \<width>

## English:
Algorithm in C that allows to generate an array of any size filled with spaces with a frame or border

Said algorithm is executed from the command console and receives two parameters, the first being the height and the second the width, resulting in the drawing of the empty frame or square with said proportions.

### Usage
There are two ways to only execute the algorithm:

1. **af**
It allows to see a small and brief description about the use of the algorithm.

2. **af \<height> \<widht>**
In this way the algorithm is passed two arguments, the first being the height and the second being the width of the frame or empty square.

	Example: `af 5 6`

	Resulting in this:
	```
	 ######
	 #    #
	 #    #
	 #    #
	 ######
	```


## Español:
Algoritmo en C que permite generar una matriz de cualquier tamaño rellena de espacios con un marco o borde.

Dicho algoritmo se ejecuta desde la consola de comandos y recibe dos parametros, el primero es la altura y el segundo la anchura, dando como resultado el dibujo del marco o cuadrado vacios con dichas proporciones.

### Uso
Existen dos formas unicamente de ejecutar el algoritmo:

1. **af**
Permite ver una pequeña y breve descripcion sobre el uso del algoritmo.

2. **af \<height> \<widht>**
De esta forma al algoritmo se le pasan dos argumentos, el primero siendo la altura y el segundo siendo la anchura del marco o cuadrado vacio. 

	Ejemplo: `af 5 6`

	Dando como resultado esto:
	```
	 ######
	 #    #
	 #    #
	 #    #
	 ######
	```
